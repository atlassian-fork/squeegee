For general documentation see: https://bitbucket.org/atlassian/squeegee/wiki/Home


# Development work (Atlassian Internal Process)

Assume CENG development account with Cloudtoken in docker daemon mode.

Development should be on the ```development``` branch.

To process a single file run:
```docker-compose run worker cur-data --mode worker```


# Development work (Public Process)

Update the contents of the docker-compose.yml file to point to s3 buckets you have access to. Pre-populated with 
sample CUR files and any mapping data you need.

To process a single file run:
```docker-compose run worker cur-data --mode worker```
