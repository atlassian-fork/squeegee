

def chunks(chunkable, n):
    for i in range(0, len(chunkable), n):
        yield chunkable[i:i + n]
