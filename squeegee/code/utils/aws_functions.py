import boto3
from utils.list_functions import chunks
import logging
import os
from time import sleep

logger = logging.getLogger(__name__)


def clean_out_s3_folder(bucket_name, bucket_region, prefix):
    try:
        if not prefix.endswith("/"):
            prefix = prefix + "/"

        s3 = boto3.resource("s3", region_name=bucket_region)
        bucket = s3.Bucket(bucket_name)
        for chunk in chunks([{"Key": f.key} for f in bucket.objects.filter(Prefix=prefix)], 1000):
            bucket.delete_objects(Delete={"Objects": chunk})
    except Exception as e:
        logger.exception(e)
        return False
    return True


def start_batch_job(
    command,
    environment,
    parameters=None,
    vcpus=1,
    memory=256,
    retries=1,
    array_size=None,
    timeout_seconds=None,
    depends_on=None,
):
    batch_region = os.environ.get("AWS_REGION", None)
    batch_job_definition = os.environ.get("BATCH_JOB_DEFINITION", None)
    batch_job_queue = os.environ.get("BATCH_JOB_QUEUE", None)
    environment.append({"name": "BATCH_JOB_DEFINITION", "value": batch_job_definition})

    if not batch_region:
        logger.error("Unable to get Batch region from environment")
        return False

    if not batch_job_definition:
        logger.error("Unable to get Batch job definition from environment")
        return False

    if not batch_job_queue:
        logger.error("Unable to get Batch job queue from environment")
        return False

    kwargs = {
        "jobName": "CURTransform",
        "jobQueue": batch_job_queue,
        "jobDefinition": batch_job_definition,
        "containerOverrides": {"vcpus": vcpus, "memory": memory, "command": command, "environment": environment},
        "retryStrategy": {"attempts": retries},
    }
    if parameters:
        kwargs["parameters"] = parameters
    if array_size:
        kwargs["arrayProperties"] = {"size": array_size}
    if timeout_seconds:
        kwargs["timeout"] = {"attemptDurationSeconds": timeout_seconds}
    if depends_on:
        kwargs["dependsOn"] = depends_on
    client = boto3.client("batch", region_name=batch_region)
    response = client.submit_job(**kwargs)
    return response


def assume_role(role_arn, role_session_name="Squeegee"):
    batch_region = os.environ.get("AWS_REGION", None)
    sts = boto3.client(service_name="sts", region_name=batch_region)
    credentials = sts.assume_role(RoleArn=role_arn, RoleSessionName=role_session_name)
    session = boto3.Session(
        aws_session_token=credentials["Credentials"]["SessionToken"],
        aws_access_key_id=credentials["Credentials"]["AccessKeyId"],
        aws_secret_access_key=credentials["Credentials"]["SecretAccessKey"],
    )
    return session


def upsert_crawler(glue, crawler_name, args):
    try:
        response = glue.update_crawler(**args)
    except Exception:
        logger.info("Crawler {} not found, creating new one.".format(crawler_name))
        response = glue.create_crawler(**args)
    return response


def get_status_from_crawler_response(response):
    crawler_details = response.get("Crawler", {})
    state = crawler_details.get("State", "NA")
    return state


def crawler_running(glue, crawler_name):
    response = glue.get_crawler(Name=crawler_name)
    return get_status_from_crawler_response(response) != "READY"


def wait_for_crawler(glue, crawler_name):
    while crawler_running(glue, crawler_name):
        sleep(30)


def run_crawler(glue, crawler_name):
    if crawler_running(glue, crawler_name):
        result = wait_for_crawler(glue, crawler_name)
        if not result:
            return False
    start_crawler(glue, crawler_name)
    wait_for_crawler(glue, crawler_name)
    return True


def start_crawler(glue, crawler_name):
    # Start glue crawler
    glue.start_crawler(Name=crawler_name)


def send_notification(sns_topic, region, message):
    sns = boto3.client("sns", region_name=region)
    try:
        sns.publish(TopicArn=sns_topic, Message=message, Subject="Squeegee Notification")
    except Exception as e:
        logger.exception(e)
