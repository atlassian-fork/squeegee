import os


def load_environment():
    environment = dict()
    try:
        environment['retry_count'] = int(os.environ.get('AWS_BATCH_JOB_ATTEMPT', '0'))
    except ValueError:
        environment['retry_count'] = 0
    environment['current_region'] = os.environ.get('AWS_REGION', 'us-east-1')
    environment['manifest_bucket'] = os.environ.get('MANIFEST_BUCKET', False)
    environment['manifest_key'] = os.environ.get('MANIFEST_KEY', False)
    environment['cur_bucket'] = os.environ.get('CUR_BUCKET', False)
    environment['cur_bucket_region'] = os.environ.get('CUR_BUCKET_REGION', 'us-east-1')
    environment['output_prefix'] = os.environ.get('OUTPUT_PREFIX', None)
    environment['output_bucket'] = os.environ.get('ANALYTICS_BUCKET', False)
    environment['output_bucket_region'] = os.environ.get('ANALYTICS_BUCKET_REGION', 'us-east-1')
    environment['cur_data_key_prefix'] = os.environ.get('CUR_DATA_KEY_PREFIX', 'aws_cost_usage_report')
    environment['pricing_data_key_prefix'] = os.environ.get('PRICING_DATA_KEY_PREFIX', 'aws_pricing')
    environment['mappings_prefix'] = os.environ.get('MAPPINGS_PREFIX', 'mappings')

    environment['glue_management_role'] = os.environ.get('GLUE_MANAGEMENT_ROLE', None)
    environment['glue_crawler_role'] = os.environ.get('GLUE_CRAWLER_ROLE', None)
    environment['glue_crawler_name_prefix'] = os.environ.get('GLUE_CRAWLER_NAME_PREFIX', 'AWS-')
    environment['glue_crawler_table_prefix'] = os.environ.get('GLUE_CRAWLER_TABLE_PREFIX', '')
    environment['glue_region'] = os.environ.get('GLUE_REGION', 'us-east-1')
    environment['datacatalog_db_name'] = os.environ.get('DATACATALOG_DB_NAME', 'default')

    try:
        environment['cur_key_length'] = int(os.environ.get('CUR_KEY_LENGTH', ''))
    except ValueError:
        pass

    try:
        environment['batch_job_array_index'] = int(os.environ.get('AWS_BATCH_JOB_ARRAY_INDEX', ''))
    except ValueError:
        pass
    environment['cur_key_prefix'] = os.environ.get('CUR_KEY_PREFIX', None)
    environment['cur_folder'] = os.environ.get('CUR_FOLDER', None)
    environment['year'] = os.environ.get('YEAR', None)
    environment['month'] = os.environ.get('MONTH', None)
    environment['notification_sns'] = os.environ.get('SQUEEGEE_NOTIFICATIONS_SNS', None)

    return environment
