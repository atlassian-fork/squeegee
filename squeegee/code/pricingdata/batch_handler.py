from datetime import datetime
import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd
import requests
import s3fs
import logging
from utils import pandas_functions
from utils import aws_functions
from utils import envrionment_functions


logger = logging.getLogger(__name__)


def parser(subparser):
    subparser.add_argument("-m", "--mode",
                           help="Run in specified mode",
                           action="store",
                           choices=["worker"]
                           )


def runner(args):
    if args.mode == 'worker':
        logger.info('Starting pricing-data stack worker')
        return worker()
    return False


def worker():
    environment = envrionment_functions.load_environment()
    url_base = 'https://pricing.us-east-1.amazonaws.com'
    offers_url = '{base}/offers/v1.0/aws/index.json'.format(base=url_base)
    r = requests.get(offers_url)

    df = pd.DataFrame()
    for offer_code, offer in r.json()['offers'].items():
        url = f"{url_base}{offer['currentVersionUrl'].replace('.json', '.csv')}"
        logger.info(f"Downloading {url}")
        tmp = pd.read_csv(url,
                          skiprows=5,
                          dtype=object)
        tmp.offer_code = offer_code
        tmp = pandas_functions.fix_column_names(tmp)
        logger.info("Appending to data_set")
        df = df.append(tmp)

    # noinspection PyArgumentList,PyArgumentList
    table = pa.Table.from_pandas(df, preserve_index=False)

    s3 = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={'region_name': environment['output_bucket_region']})
    year = datetime.utcnow().year
    month = datetime.utcnow().month
    day = datetime.utcnow().day
    with s3.open(f"{environment['output_bucket']}/{environment['pricing_data_key_prefix']}/year={year}/month={month}/pricelist.parquet", 'wb') as f:  # noqa
        pq.write_table(table, f)
    run_crawler(environment)
    return True


def run_crawler(environment):
    glue_role_arn = environment['glue_management_role']
    glue_crawler_region = environment['glue_region']
    glue_crawler_name = f"{environment['glue_crawler_name_prefix']}PricingDataCrawler"

    role = aws_functions.assume_role(role_arn=glue_role_arn)
    glue = role.client(service_name='glue',
                       region_name=glue_crawler_region)

    glue_s3_target = f"s3://{environment['output_bucket']}/{environment['pricing_data_key_prefix']}"
    args = {
        'Name': glue_crawler_name,
        'Role': environment['glue_crawler_role'],
        'DatabaseName': environment['datacatalog_db_name'],
        'TablePrefix': environment['glue_crawler_table_prefix'],
        'Description': 'Pricing Data Crawler',
        'Targets': {'S3Targets': [{"Exclusions": [], "Path": glue_s3_target}]},
        'SchemaChangePolicy': {'DeleteBehavior': 'DELETE_FROM_DATABASE',
                               'UpdateBehavior': 'UPDATE_IN_DATABASE'}
    }

    if aws_functions.upsert_crawler(glue, glue_crawler_name, args):
        aws_functions.start_crawler(glue, glue_crawler_name)
    else:
        logger.error('Upsert crawler failed - refusing to run crawler')
        return False
    return True
