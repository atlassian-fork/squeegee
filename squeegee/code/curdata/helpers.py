import re
from curdata.lambda_handler import manifest_key_regex
import s3fs
import json
import os
from curdata.mapping_handler import load_mappings
from curdata.transformations import apply_df_transforms
from collections import defaultdict
import logging
from time import sleep
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
from utils.pandas_functions import fix_column_names
from utils import aws_functions
import boto3


# This was a nice idea but until PyArrow and Athena get more supported types it
# is useless for us. I'll leave it here in dreams of one day using it.
column_format_mapping = defaultdict(pa.string)
# column_format_mapping[''] = pa.decimal128(precision=15, scale=30)

logger = logging.getLogger(__name__)


def get_year_month_from_manifest_key(manifest_key):
    g = re.match(manifest_key_regex, manifest_key)
    if g:
        return g.group("year"), g.group("month")
    return None, None


def get_manifest(cur_bucket, cur_bucket_region, manifest_key):
    if manifest_key.endswith(".json"):
        s3 = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={"region_name": cur_bucket_region})
        in_file = "s3://{bucket}/{key}".format(bucket=cur_bucket, key=manifest_key)
        with s3.open(in_file, "rb") as f:
            return json.loads(f.read())
    else:
        return None


def get_cur_folder_from_key(key_name):
    return key_name.rsplit("/", 1)[0]


def get_cur_numbering_length(key_name):
    # Method assumes the manifest numbering is as follows:
    # key_prefix/cur_report_name-##.csv.gz
    # We are trying to work out how many digits the cur file number is.
    # If you receive < 10 files AWS number them as 1.csv.gz (We want to get a length of 1)
    # If you receive > 10 < 100 AWS number them as 01.csv.gz (We want to get a length of 2)
    # If you receive > 100 < 1000 AWS number them as 001.csv.gz (We want to get a length of 3)
    # So on.
    return int(len(key_name.split("/")[-1].split("-")[-1].split(".")[0]))


def get_cur_file_prefix(key_name):
    length = get_cur_numbering_length(key_name)
    logger.info(f"Found key length of {length} for key {key_name}")
    m = re.search(f"(.*-)[0-9]{{{length}}}.csv.gz", os.path.basename(key_name))
    return m.group(1)


def get_cur_name(environment):
    # batch_job_array_index+1 index padded with zeros to the cur_key_length.
    # Eg. Where batch_job_array_index=5 and cur_key_length=3
    #     index_id = '005'
    batch_job_array_index = environment["batch_job_array_index"]
    batch_job_array_index += 1
    index_id = f"{batch_job_array_index:0{environment['cur_key_length']}}"
    return f"{environment['cur_key_prefix']}{index_id}.csv.gz"


def write_parquet(input_df, out_file_handle, in_filename, out_filename, mappings):
    writer = None
    try:
        count = 0
        lines_written = 0
        for chunk in input_df:
            logger.debug("DF Chunk Memory Usage: %d", chunk.memory_usage(index=True).sum())
            count += 1
            logger.info(f"Reading chunk ({count})")
            chunk = fix_column_names(chunk)
            apply_df_transforms(chunk, mappings, in_filename, out_filename)

            # noinspection PyArgumentList,PyArgumentList
            tb = pa.Table.from_pandas(df=chunk, preserve_index=False, nthreads=2)
            lines_written += tb.num_rows
            logger.info(f"Writing {tb.num_rows} rows.")
            if not writer:
                columns = [pa.field(c, column_format_mapping[c]) for c in chunk.columns]
                schema = pa.schema(columns)
                writer = pq.ParquetWriter(out_file_handle, schema, compression="snappy")
            logger.info("Writing chunk ({id})".format(id=count))
            writer.write_table(tb)
    finally:
        logger.info(f"Wrote out {lines_written} lines to parquet.")
        if writer and writer.is_open:
            writer.close()
    return lines_written


def check_lines_written(s3, lines_written, source_bucket, source_key):
    response = s3.select_object_content(
        Bucket=source_bucket,
        Key=source_key,
        ExpressionType="SQL",
        Expression="select count(*) from s3object",
        InputSerialization={"CSV": {}, "CompressionType": "GZIP"},
        OutputSerialization={"CSV": {}},
    )
    line_count = 0
    for event in response["Payload"]:
        if "Records" in event:
            line_count = int(event["Records"]["Payload"].decode("utf-8")) - 1
    logger.info(f"Wrote out {lines_written} from source file {source_key}, expected {line_count}")
    return lines_written == line_count


def transform_file(environment, source_key, mappings):
    source_bucket = environment["cur_bucket"]
    destination_bucket = environment["output_bucket"]
    cur_name = get_cur_name(environment)
    destination_key = f"{environment['output_prefix']}/{cur_name.replace('.csv.gz', '.parquet')}"

    in_file = f"s3://{source_bucket}/{source_key}"
    logger.info(f"Reading from: {in_file}")

    out_file = f"s3://{destination_bucket}/{destination_key}"
    logger.info(f"Writing to: {out_file}")

    source_bucket_region = environment["cur_bucket_region"]
    destination_bucket_region = environment["output_bucket_region"]
    s3_in = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={"region_name": source_bucket_region})
    s3_out = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={"region_name": destination_bucket_region})
    s3 = boto3.client("s3", region_name=source_bucket_region)
    try:
        # Sometimes we get 404 errors from s3, we need to wait for AWS to complete the upload.
        retries = 120
        while not s3_in.exists(in_file):
            if retries == 0:
                logger.error(f"File {in_file} does not exist yet, giving up waiting")
                return False
            logger.warning(f"File {in_file} does not exist yet, waiting...")
            sleep(5)
            retries -= 1

        with s3_in.open(in_file, "rb") as f_in, s3_out.open(out_file, "wb") as f_out:
            # Chunksize chosen arbitrarily, over 200K seems to blow out the memory usage.
            # over 100K doesn't seem to decrease duration, but having chunks increase
            # parquet compression up to 64 * 1024 * 1024
            df = pd.read_csv(f_in, skiprows=0, compression="gzip", dtype=object, iterator=True, chunksize=150000)
            # Write out to S3.
            lines_written = write_parquet(df, f_out, in_file, out_file, mappings)
            if not check_lines_written(s3, lines_written, source_bucket, source_key):
                logger.error("Number of lines written does not match source file.")
                return False
    except Exception as e:
        if s3_out and out_file:
            # If we encounter an exception while writing or transforming a file.
            # We want to remove any partially written data. This has the benefit
            # that it will fail our validate function so we will see the overall
            # execution fail as well.
            s3_out.rm(out_file)

        logger.exception(e)
        return False
    return True


def invoke_transform_file(environment):
    key_path = f"{environment['cur_folder']}/{get_cur_name(environment)}"
    mappings = load_mappings(
        environment["year"], environment["month"], environment["output_bucket"], environment["output_bucket_region"]
    )
    return transform_file(environment, key_path, mappings)


def run_crawler(environment):
    glue_role_arn = environment["glue_management_role"]
    glue_crawler_region = environment["glue_region"]
    glue_crawler_name = f"{environment['glue_crawler_name_prefix']}CURDataCrawler"

    role = aws_functions.assume_role(role_arn=glue_role_arn)
    glue = role.client(service_name="glue", region_name=glue_crawler_region)

    glue_s3_target = f"s3://{environment['output_bucket']}/{environment['cur_data_key_prefix']}"
    args = {
        "Name": glue_crawler_name,
        "Role": environment["glue_crawler_role"],
        "DatabaseName": environment["datacatalog_db_name"],
        "TablePrefix": environment["glue_crawler_table_prefix"],
        "Description": "Cost and Usage Report Data Crawler",
        "Targets": {"S3Targets": [{"Exclusions": [], "Path": glue_s3_target}]},
        "SchemaChangePolicy": {"DeleteBehavior": "DELETE_FROM_DATABASE", "UpdateBehavior": "UPDATE_IN_DATABASE"},
    }

    if aws_functions.upsert_crawler(glue, glue_crawler_name, args):
        if not aws_functions.run_crawler(glue, glue_crawler_name):
            logger.error("Run crawler failed")
            return False
    else:
        logger.error("Upsert crawler failed - refusing to run crawler")
        return False
    return True
