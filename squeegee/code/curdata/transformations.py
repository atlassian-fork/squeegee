import numpy as np
import re


def long_format_account_id(account_id):
    """Convert AWS Account Ids from string of numbers into 3 groups of numbers separated by -
    
    :param account_id: The AWS Account Id to format.
    :type account_id: string
    :return: Formatted Account Id
    :rtype: string
    """
    return f"{account_id[0:4]}-{account_id[4:8]}-{account_id[8:]}"


def apply_mapping(df, index_key, mappings, mapping_key, destination_key, lowercase=False):
    """Apply mapping to dataframe.
    
    :param df: The dataframe to apply mapping to.
    :type df: pd.Dataframe
    :param index_key: The column name to use in the dataframe to map values with.
    :type index_key: string
    :param mappings: The full set of mappings. As loaded by the mapping handler.
    :type mappings: dict
    :param mapping_key: The key for the particular mapping to use.
    :type mapping_key: string
    :param destination_key: The name of the (possibly new) column to map data into the dataframe.
    :type destination_key: string
    :param lowercase: Should the mapping keys be made lowercase for this mapping, defaults to False
    :type lowercase: bool, optional
    """
    if mapping_key in mappings:
        if lowercase:
            mapping = dict((k.lower(), v) for k, v in mappings[mapping_key].items())
        else:
            mapping = mappings[mapping_key]
        df[destination_key] = df[index_key].map(mapping, na_action="ignore")
        df[destination_key] = df[destination_key].replace(r"^\s*$", np.nan, regex=True)
    else:
        print(f"Skipping mapping for {mapping_key}")


def apply_mapping_list(df, mappings, mapping_list):
    # For each of the mappings in the list apply it.
    for mapping in mapping_list:
        apply_mapping(
            df=df,
            index_key=mapping["index_key"],
            mappings=mappings,
            mapping_key=mapping["mapping_key"],
            destination_key=mapping["destination_key"],
            lowercase=mapping.get("lowercase", False),
        )


def apply_df_cleanup(df, mappings, in_filename, out_filename):
    # Add columns to help identify what file each line of data is from.
    df["source_file"] = out_filename
    df["original_source_file"] = in_filename

    # Strip leading and trailing whitespace from tag data.
    for key in [col for col in df.columns if col.startswith("resourcetags_user_")]:
        df[key] = df[key].str.strip()

    # If AWS Tag business_unit is in the dataframe, create a lowercase version of this tag for mapping.
    if "resourcetags_user_business_unit" in df:
        df["resourcetags_user_business_unit_lower"] = df["resourcetags_user_business_unit"].str.lower()

    # Vectorize the long_format_account_id function to speed up execution.
    format_account_id = np.vectorize(long_format_account_id)
    # Apply formatting to the AWS Account Id and generate new column account_id_long.
    df["account_id_long"] = format_account_id(df["lineitem_usageaccountid"])


def remove_temp_columns(df):
    # Remove all tmp_* columns created during the transforms.
    for column in df.columns:
        if column.startswith("tmp_"):
            df.drop(column, axis=1, inplace=True)


def apply_df_transforms(df, mappings, in_filename, out_filename):
    """Apply all mappings and transforms to the dataframe before output.
    
    :param df: The dataframe to apply mapping to.
    :type df: pd.Dataframe
    :param mappings: The full set of mappings. As loaded by the mapping handler.
    :type mappings: dict
    :param in_filename: The keyname of the s3 file this dataframe was loaded from.
    :type in_filename: string
    :param out_filename: The keyname of the s3 file this dataframe will be output to.
    :type out_filename: string
    """
    apply_df_cleanup(df, mappings, in_filename, out_filename)

    # Apply base mappings
    mapping_list = [
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "account_names__account_id_to_account_name",
            "destination_key": "account_name",
        },
        {
            "index_key": "product_region",
            "mapping_key": "geo_locations__short_name_to_geo_location",
            "destination_key": "geo_location",
        },
        {
            "index_key": "product_region",
            "mapping_key": "airport_codes__short_name_to_airport",
            "destination_key": "airport_code",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_product",
            "destination_key": "allocation_data_account_product",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_unspecified_product",
            "destination_key": "allocation_data_account_unspecified_product",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_unspecified_fg",
            "destination_key": "allocation_data_account_unspecified_fg",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_env_map",
            "destination_key": "allocation_data_account_env_map",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_fg",
            "destination_key": "allocation_data_account_fg",
        },
        {
            "index_key": "product_productname",
            "mapping_key": "aws_product_group__aws_product_to_aws_product_group",
            "destination_key": "cost_allocation_aws_product",
        },
        {
            "index_key": "product_region",
            "mapping_key": "region_group__region_to_region_group",
            "destination_key": "cost_allocation_region",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_force_default_bu",
            "destination_key": "allocation_data_force_default_bu",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "default_cost_centers__account_id_to_default_business_unit",
            "destination_key": "default_business_unit",
        },
        {
            "index_key": "lineitem_usageaccountid",
            "mapping_key": "nocm_account_mappings__account_id_to_account_group",
            "destination_key": "allocation_data_account_group",
        },
    ]
    apply_mapping_list(df, mappings, mapping_list)

    # Set null cost_allocation_aws_product to 'Other
    df.loc[df["cost_allocation_aws_product"].isnull(), "cost_allocation_aws_product"] = "Other"
    # Set null cost_allocation_region to 'Other
    df.loc[df["cost_allocation_region"].isnull(), "cost_allocation_region"] = "Other"

    if "default_business_unit" in df:
        # Generate a lowercase version for mapping later.
        df["default_business_unit_lower"] = df["default_business_unit"].str.lower()

    # Service cost group is:
    # Value of tag service_cost_group
    # Value of micros_service_id
    # Value of service_name
    # First match wins.
    if "resourcetags_user_service_cost_group" in df:
        if "resourcetags_user_micros_service_id" in df:
            if "resourcetags_user_service_name" in df:
                df["resourcetags_user_service_cost_group"] = (
                    df["resourcetags_user_service_cost_group"]
                    .combine_first(df["resourcetags_user_micros_service_id"])
                    .combine_first(df["resourcetags_user_service_name"])
                )
    else:
        df["resourcetags_user_service_cost_group"] = np.nan

    # service_name_short is service_cost_group split on '.' and '--'. First component wins.
    if "resourcetags_user_service_cost_group" in df:
        df["allocation_data_service_name_short"] = df["resourcetags_user_service_cost_group"].str.split(".").str[0]
        df["allocation_data_service_name_short"] = df["allocation_data_service_name_short"].str.split("--").str[0]

    # Add serviceIndex for mapping
    if (
        "allocation_data_account_group" in df
        and "allocation_data_service_name_short" in df
        and "lineitem_usageaccountid" in df
    ):
        # Convert nulls to empty strings.
        df.loc[df["allocation_data_service_name_short"].isnull(), "allocation_data_service_name_short"] = ""
        df.loc[df["allocation_data_account_group"].isnull(), "allocation_data_account_group"] = ""

        # ServiceIndex is service_name_short with the account_group appended.
        df["allocation_data_service_index"] = (
            df["allocation_data_service_name_short"].str.lower() + df["allocation_data_account_group"].str.lower()
        )

    # List of mappings that need to be applied.
    mapping_list = [
        {
            "index_key": "allocation_data_service_index",
            "mapping_key": "nocm_service_mappings__service_index_to_fg",
            "destination_key": "allocation_data_service_fg",
            "lowercase": True,
        },
        {
            "index_key": "allocation_data_service_index",
            "mapping_key": "nocm_service_mappings__service_index_to_service_group",
            "destination_key": "allocation_data_service_group",
            "lowercase": True,
        },
        {
            "index_key": "allocation_data_service_index",
            "mapping_key": "nocm_service_mappings__service_index_to_product",
            "destination_key": "allocation_data_service_product",
            "lowercase": True,
        },
        {
            "index_key": "default_business_unit_lower",
            "mapping_key": "department_to_fg__department_to_functional_group",
            "destination_key": "allocation_data_default_business_unit_fg",
            "lowercase": True,
        },
        {
            "index_key": "allocation_data_service_group",
            "mapping_key": "service_cost_group_to_bu__service_cost_group_to_business_unit",
            "destination_key": "allocation_data_special_cost_allocation",
        },
    ]
    apply_mapping_list(df, mappings, mapping_list)

    # If resourcetags_user_business_unit and allocation_data_service_name_short is empty set to "Untagged"
    # If service_group is null set it to "Not Specified", If it is "0", set it to the service_name_short

    df.loc[df["allocation_data_service_group"].isnull(), "allocation_data_service_group"] = "Not Specified"
    df.loc[
        (df["allocation_data_service_name_short"] == "") & (df["resourcetags_user_business_unit"].isnull()),
        "allocation_data_service_group",
    ] = "Untagged"

    df.loc[df["allocation_data_service_group"] == "0", "allocation_data_service_group"] = df[
        "allocation_data_service_name_short"
    ]

    # If service product is "0" set it to "Unmapped"
    df.loc[df["allocation_data_service_product"] == "0", "allocation_data_service_product"] = "Unmapped"

    if "resourcetags_user_environment" in df and "resourcetags_user_env" in df:
        df["resourcetags_user_environment"] = df["resourcetags_user_environment"].combine_first(
            df["resourcetags_user_env"]
        )

    # Cost allocation environment is in order of preference
    # Account environment
    # Prod if contains prod
    # Staging if contains stg or stag
    # Dev is contains dev, test, or play
    # First match wins.
    df.loc[df["allocation_data_account_env_map"].notnull(), "cost_allocation_environment"] = df[
        "allocation_data_account_env_map"
    ]
    df.loc[
        (df["account_name"].notnull())
        & (df["account_name"].astype(str).str.contains("prod", flags=re.IGNORECASE))
        & (df["allocation_data_account_env_map"].isnull()),
        "cost_allocation_environment",
    ] = "Prod"
    df.loc[
        (df["account_name"].notnull())
        & (
            (df["account_name"].astype(str).str.contains("stg", flags=re.IGNORECASE))
            | df["account_name"].astype(str).str.contains("stag", flags=re.IGNORECASE)
        )
        & (df["allocation_data_account_env_map"].isnull()),
        "cost_allocation_environment",
    ] = "Stg"
    df.loc[
        (df["account_name"].notnull())
        & (
            (df["account_name"].astype(str).str.contains("dev", flags=re.IGNORECASE))
            | (df["account_name"].astype(str).str.contains("test", flags=re.IGNORECASE))
            | (df["account_name"].astype(str).str.contains("play", flags=re.IGNORECASE))
        )
        & (df["allocation_data_account_env_map"].isnull()),
        "cost_allocation_environment",
    ] = "Dev"

    if (
        "pricing_publicondemandrate" in df
        and "lineitem_unblendedrate" in df
        and "lineitem_lineitemtype" in df
        and "product_productname" in df
    ):
        # This is to fix up some services lines as they publish zero value rates
        df.loc[df["pricing_publicondemandcost"] == "0.0000000000", "pricing_publicondemandcost"] = None
        df.loc[df["pricing_publicondemandrate"] == "0.0000000000", "pricing_publicondemandrate"] = None

        # This will copy the public ondemand rate/cost if its not filled in use our actual rate/cost
        df["shelf_rate"] = df["pricing_publicondemandrate"].combine_first(df["lineitem_unblendedrate"])
        df["shelf_cost"] = df["pricing_publicondemandcost"].combine_first(df["lineitem_unblendedcost"])

    if (
        "reservation_reservationarn" in df
        and "pricing_publicondemandcost" in df
        and "pricing_publicondemandrate" in df
        and "lineitem_unblendedrate" in df
        and "lineitem_unblendedcost" in df
        and "lineitem_lineitemtype" in df
    ):
        df.loc[
            (df["reservation_reservationarn"].notnull()) | (df["lineitem_lineitemtype"] == "DiscountedUsage"),
            "unblendedcost_withoutri",
        ] = df["pricing_publicondemandcost"]
        df["unblendedcost_withoutri"] = df["unblendedcost_withoutri"].combine_first(df["lineitem_unblendedcost"])
        df.loc[df["lineitem_lineitemtype"].isin(["RIFee"]), "unblendedcost_withoutri"] = "0.0000000000"
        df.loc[df["lineitem_lineitemtype"].isin(["SavingsPlanNegation"]), "unblendedcost_withoutri"] = "0.0000000000"
        df.loc[
            df["lineitem_lineitemtype"].isin(["SavingsPlanRecurringFee"]), "unblendedcost_withoutri"
        ] = "0.0000000000"
        df.loc[
            (df["lineitem_lineitemtype"].isin(["Fee"])) & (df["reservation_reservationarn"].notnull()),
            "unblendedcost_withoutri",
        ] = "0.0000000000"

        df.loc[
            (df["reservation_reservationarn"].notnull()) | (df["lineitem_lineitemtype"] == "DiscountedUsage"),
            "unblendedrate_withoutri",
        ] = df["pricing_publicondemandrate"]
        df["unblendedrate_withoutri"] = df["unblendedrate_withoutri"].combine_first(df["lineitem_unblendedrate"])
        df.loc[df["lineitem_lineitemtype"].isin(["RIFee"]), "unblendedrate_withoutri"] = "0.0000000000"
        df.loc[
            (df["lineitem_lineitemtype"].isin(["Fee"])) & (df["reservation_reservationarn"].notnull()),
            "unblendedrate_withoutri",
        ] = "0.0000000000"

    # Atlassian cost allocation rules.
    if (
        "resourcetags_user_business_unit" in df
        and "bad_business_unit_mapping__business_unit_to_Department" in mappings
        and "default_cost_centers__account_id_to_default_business_unit" in mappings
    ):
        # Apply the default_bu when we want to force it.
        df.loc[df["allocation_data_force_default_bu"] == "TRUE", "allocation_data_forced_cost_allocation"] = df[
            "default_business_unit"
        ]

        df["resourcetags_user_cost_allocation"] = (
            df["allocation_data_special_cost_allocation"]
            .combine_first(df["allocation_data_forced_cost_allocation"])
            .combine_first(df["resourcetags_user_business_unit"])
            .combine_first(df["default_business_unit"])
        )
        # generate temp lowercase column
        df["tmp_resourcetags_user_cost_allocation_lower"] = df["resourcetags_user_cost_allocation"].str.lower()

        # then lookup the values in the bad mapping map.
        apply_mapping(
            df=df,
            index_key="tmp_resourcetags_user_cost_allocation_lower",
            mappings=mappings,
            mapping_key="bad_business_unit_mapping__business_unit_to_Department",
            destination_key="tmp_bad_tag_corrections",
            lowercase=True,
        )

        # apply the default business unit, if the bad tag mapping maps to a literal '0'
        df.loc[df["tmp_bad_tag_corrections"] == "0", "tmp_bad_tag_corrections"] = df["default_business_unit"]

        # lastly, combine the cost allocations that where not in the map.
        df["resourcetags_user_cost_allocation"] = df["tmp_bad_tag_corrections"].combine_first(
            df["resourcetags_user_cost_allocation"]
        )

        # Update lowercase cost allocation for mapping
        df["allocation_data_cost_allocation_lower"] = df["resourcetags_user_cost_allocation"].str.lower()

        # Map the cost allocation value to the department fg.
        mapping_list = [
            {
                "index_key": "allocation_data_cost_allocation_lower",
                "mapping_key": "department_to_fg__department_to_functional_group",
                "destination_key": "allocation_data_cost_allocation_fg",
                "lowercase": True,
            }
        ]
        apply_mapping_list(df, mappings, mapping_list)

        # Cost allocation fg is in order or preference:
        # Account FG
        # Service FG
        # Account Unspecified FG
        # Cost Allocation FG
        # Default BU FG
        # First match wins.
        df["cost_allocation_fg"] = (
            df["allocation_data_account_fg"]
            .combine_first(df["allocation_data_service_fg"])
            .combine_first(df["allocation_data_account_unspecified_fg"])
            .combine_first(df["allocation_data_cost_allocation_fg"])
            .combine_first(df["allocation_data_default_business_unit_fg"])
        )

        # If any resources are allocated to COGS by the service or account then
        # Apply COGS department mapping
        mask = df["cost_allocation_fg"] == "COGS"
        df_cogs = df[mask]
        mapping = dict(
            (k.lower(), v) for k, v in mappings["bu_to_cogs_department__business_unit_to_cogs_department"].items()
        )
        df.loc[mask, "resourcetags_user_cost_allocation"] = (
            df_cogs["allocation_data_cost_allocation_lower"]
            .map(mapping)
            .combine_first(df_cogs["resourcetags_user_cost_allocation"])
        )
        df["cost_allocation_business_unit"] = df["resourcetags_user_cost_allocation"]

    # Handle product special cases
    if "nocm_product_special_case_mappings__service_bu_index_to_product" in mappings:
        df["tmp_service_bu"] = (
            df["allocation_data_service_name_short"].str.lower()
            + df["allocation_data_cost_allocation_lower"].str.lower()
        )
        apply_mapping(
            df=df,
            index_key="tmp_service_bu",
            mappings=mappings,
            mapping_key="nocm_product_special_case_mappings__service_bu_index_to_product",
            destination_key="cost_allocation_product_override",
            lowercase=True,
        )

    # Cost allocation product is in order of preference:
    # Overrides
    # Account product
    # Service product
    # Account Unspecified Product
    # First match wins.
    df["cost_allocation_product"] = (
        df["cost_allocation_product_override"]
        .combine_first(df["allocation_data_account_product"])
        .combine_first(df["allocation_data_service_product"])
        .combine_first(df["allocation_data_account_unspecified_product"])
    )

    remove_temp_columns(df)
