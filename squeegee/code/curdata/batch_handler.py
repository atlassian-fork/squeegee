import logging
from curdata.lambda_handler import validate_environment_variables
from curdata.mapping_handler import stage_mapping_files
import curdata.helpers as helpers
from dyndbmutex.dyndbmutex import DynamoDbMutex
from utils import number_functions
from utils import aws_functions
from utils import envrionment_functions
import os
import boto3

logger = logging.getLogger(__name__)


def parser(subparser):
    subparser.add_argument(
        "-m", "--mode", help="Run in specified mode", action="store", choices=["init-workers", "worker", "post-tasks"]
    )


def runner(args):
    if args.mode == "init-workers":
        logger.info("Starting cur-data stack runner")
        return init_workers()
    elif args.mode == "worker":
        logger.info("Starting cur-data stack worker")
        return worker()
    elif args.mode == "post-tasks":
        logger.info("Starting cur-data stack post-tasks")
        return post_tasks()
    return False


def set_squeegee_status(bucket_name, bucket_region, prefix, status):
    try:
        if prefix.endswith("/"):
            prefix = prefix[0:-1]
        prefix = prefix + "_status/"
        s3 = boto3.resource("s3", region_name=bucket_region)
        logger.info(f"Writing Squeegee status {status} into s3://{bucket_name}/{prefix}status.csv ")
        bucket = s3.Bucket(bucket_name)
        bucket.put_object(ACL="bucket-owner-full-control", Body=status.encode("utf-8"), Key=f"{prefix}status.csv")
    except Exception as e:
        logger.exception(e)
        return False
    return True


def init_workers():
    environment = envrionment_functions.load_environment()
    times = number_functions.ordinal(environment["retry_count"])
    logger.info(f"Running init-workers for the {times} time.")

    if not validate_environment_variables(
        logger, environment["manifest_bucket"], environment["manifest_key"], environment["cur_bucket"]
    ):
        return False

    year, month = helpers.get_year_month_from_manifest_key(environment["manifest_key"])
    if not year or not month:
        logger.error("Unable to get year/month from manifest key")
        return False

    manifest = helpers.get_manifest(
        environment["cur_bucket"], environment["cur_bucket_region"], environment["manifest_key"]
    )

    if "reportKeys" not in manifest or not manifest["reportKeys"]:
        logger.error("Unable to find reportKeys or empty reportKeys found in manifest")
        return False

    report_keys = manifest["reportKeys"]
    key_length = helpers.get_cur_numbering_length(report_keys[0])
    cur_report_folder = helpers.get_cur_folder_from_key(report_keys[0])
    cur_key_prefix = helpers.get_cur_file_prefix(report_keys[0])
    number_of_cur_files = len(manifest["reportKeys"])
    logger.info(f'Getting (DynamoDbMutex) lock on manifest {environment["manifest_key"]}')
    mutex = DynamoDbMutex(
        environment["manifest_key"], "CURDataTransform", (60 * 60 * 1000), region_name=environment["current_region"]
    )
    locked = mutex.lock()
    if not locked:
        logger.error(
            f'Failed to get (DynamoDbMutex) lock on manifest {environment["manifest_key"]} - Squeegee already running?'
        )  # noqa
        return False
    logger.info(f'Got (DynamoDbMutex) lock on manifest {environment["manifest_key"]}')

    set_squeegee_status(
        environment["output_bucket"], environment["output_bucket_region"], environment["cur_data_key_prefix"], "running"
    )

    logger.info("Staging mapping files")
    stage_mapping_files(
        year, month, environment["output_bucket"], environment["output_bucket_region"], environment["mappings_prefix"]
    )

    dest_path = f'{environment["cur_data_key_prefix"]}/month={year}-{month}'
    logger.info(f"Cleaning out s3 folder {dest_path}")
    if not aws_functions.clean_out_s3_folder(
        environment["output_bucket"], environment["output_bucket_region"], dest_path
    ):
        logger.error("Failed to clean out output directory in s3")
        mutex.release()
        return False

    try:
        response = None
        if number_of_cur_files > 1:
            command = ["cur-data", "-m", "worker"]
            batch_environment = [
                {"name": "YEAR", "value": year},
                {"name": "MONTH", "value": month},
                {"name": "CUR_KEY_LENGTH", "value": str(key_length)},
                {"name": "CUR_KEY_PREFIX", "value": cur_key_prefix},
                {"name": "CUR_FOLDER", "value": cur_report_folder},
                {"name": "OUTPUT_PREFIX", "value": dest_path},
            ]
            logger.info("Starting cur-data transform workers in Batch")
            response = aws_functions.start_batch_job(
                command=command,
                environment=batch_environment,
                vcpus=2,
                memory=8192,
                retries=2,
                array_size=number_of_cur_files,
            )
            if not response or "jobId" not in response:
                mutex.release()
                logger.error("Unable to start cur-data transform workers in Batch")
                set_squeegee_status(
                    environment["output_bucket"],
                    environment["output_bucket_region"],
                    environment["cur_data_key_prefix"],
                    "failed",
                )
                return False
            logger.info(response)
        else:
            logger.info("Starting cur-data transform inline - due to single file")
            os.environ["YEAR"] = year
            os.environ["MONTH"] = month
            os.environ["CUR_KEY_LENGTH"] = str(key_length)
            os.environ["CUR_KEY_PREFIX"] = cur_key_prefix
            os.environ["CUR_FOLDER"] = cur_report_folder
            os.environ["OUTPUT_PREFIX"] = dest_path
            os.environ["AWS_BATCH_JOB_ARRAY_INDEX"] = "0"
            if not worker():
                logger.error("Worker had errors - refusing to continue")
                set_squeegee_status(
                    environment["output_bucket"],
                    environment["output_bucket_region"],
                    environment["cur_data_key_prefix"],
                    "failed",
                )
                return False

        command = ["cur-data", "-m", "post-tasks"]
        batch_environment = [{"name": "MANIFEST_KEY", "value": environment["manifest_key"]}]

        kwargs = {"command": command, "environment": batch_environment, "vcpus": 2, "memory": 256, "retries": 2}
        if response and "jobId" in response:
            kwargs["depends_on"] = [{"jobId": response["jobId"]}]

        response = aws_functions.start_batch_job(**kwargs)
        if not response or "jobId" not in response:
            mutex.release()
            logger.error("Unable to start post-tasks in Batch")
            set_squeegee_status(
                environment["output_bucket"],
                environment["output_bucket_region"],
                environment["cur_data_key_prefix"],
                "failed",
            )
            return False
        logger.info(response)
    except Exception as e:
        logger.exception(e)
        mutex.release()
        set_squeegee_status(
            environment["output_bucket"],
            environment["output_bucket_region"],
            environment["cur_data_key_prefix"],
            "failed",
        )
        return False

    logger.info("Done")
    return True


def worker():
    environment = envrionment_functions.load_environment()
    times = number_functions.ordinal(environment["retry_count"])
    logger.info(f"Running worker for the {times} time.")
    try:
        result = helpers.invoke_transform_file(environment)
    except Exception as e:
        logger.exception(e)
        result = False

    if not result:
        logger.error("Transform for file had errors.")
        if "notification_sns" in environment and environment["notification_sns"]:
            cur_file = helpers.get_cur_name(environment)
            aws_functions.send_notification(
                environment["notification_sns"],
                environment["current_region"],
                f"Transform for file {cur_file} had errors.",
            )
            set_squeegee_status(
                environment["output_bucket"],
                environment["output_bucket_region"],
                environment["cur_data_key_prefix"],
                "failed",
            )
        return False
    logger.info("Transform for file completed.")
    return True


def post_tasks():
    environment = envrionment_functions.load_environment()
    mutex = DynamoDbMutex(
        environment["manifest_key"], "CURDataTransform", (60 * 60 * 1000), region_name=environment["current_region"]
    )
    logger.info(f"Releasing mutex lock for {environment['manifest_key']}")
    mutex.release()
    result = helpers.run_crawler(environment)
    if not result:
        logger.error("Post tasks had errors.")
        set_squeegee_status(
            environment["output_bucket"],
            environment["output_bucket_region"],
            environment["cur_data_key_prefix"],
            "failed",
        )
        return False

    set_squeegee_status(
        environment["output_bucket"],
        environment["output_bucket_region"],
        environment["cur_data_key_prefix"],
        "completed",
    )

    logger.info("Post tasks completed.")
    if "notification_sns" in environment and environment["notification_sns"]:
        aws_functions.send_notification(
            environment["notification_sns"],
            environment["current_region"],
            f"Processing for {environment['manifest_key']} completed.",
        )
    return True

