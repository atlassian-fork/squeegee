from pyathena import connect
from pytest import approx
from decimal import Decimal


def run_athena_query(query):
    cursor = connect(s3_staging_dir="s3://squeegee-ci-output/athena/").cursor()
    cursor.execute(query)
    return cursor.fetchall()


def test_account_names_mapping_applied():
    result = run_athena_query("SELECT account_name FROM squeegeeci.cost_usage_report WHERE month='2019-10' LIMIT 10")
    assert len(result) == 10


def test_all_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report WHERE month='2019-10'")
    assert result == [(63385655,)]


def test_usage_line_count():
    result = run_athena_query(
        "SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Usage' AND month='2019-10'"
    )
    assert result == [(62923605,)]


def test_discounted_usage_line_count():
    result = run_athena_query(
        "SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'DiscountedUsage' AND month='2019-10'"
    )
    assert result == [(445046,)]


def test_rifee_line_count():
    result = run_athena_query(
        "SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'RIFee' AND month='2019-10'"
    )
    assert result == [(121,)]


def test_credit_line_count():
    result = run_athena_query(
        "SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Credit' AND month='2019-10'"
    )
    assert result == [(1,)]


def test_usage_line_sum():
    result = run_athena_query(
        "SELECT SUM(CAST(lineitem_unblendedcost AS DECIMAL(30,15))) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Usage' AND month='2019-10'"
    )
    value = result[0][0]
    assert value == approx(Decimal("741534.555657868"))

