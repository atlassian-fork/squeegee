---
AWSTemplateFormatVersion: "2010-09-09"

Description: Atlassian Squeegee Base https://bitbucket.org/atlassian/squeegee

Metadata:
  "AWS::CloudFormation::Interface":
    ParameterGroups:
      - Label:
          default: "Base Squeegee Configuration"
        Parameters:
          - AnalyticsBucket
          - AnalyticsBucketRegion
          - MutexTableName
          - DockerImageName
          - CreateBatchLogGroup
      - Label:
          default: "AWS Infrastructure Configuration"
        Parameters:
          - VPCId
          - Subnets
      - Label:
          default: "Glue Configuration"
        Parameters:
          - GlueDataCatalogName
          - GlueRegion
          - GlueCrawlerNamePrefix
          - GlueCrawlerTablePrefix
          - GlueManagementRoleARN
          - GlueCrawlerRoleARN
      - Label:
          default: "Select Squeegee Stack Deployments"
        Parameters:
          - DeployPricingDataStack
          - DeployCURDataStack
      - Label:
          default: "Cost and Usage Report Stack Configuration"
        Parameters:
          - CURBucket
          - CURBucketRegion
          - CURDataKeyPrefix
          - CURDropColumns

Parameters:
  AnalyticsBucket:
    Description: The S3 bucket to load the data into
    Type: String
  AnalyticsBucketRegion:
    Description: The S3 bucket region to load the data into
    Type: String
    Default: us-east-1

  DeployCURDataStack:
    Type: String
    Default: true
    AllowedValues: ['true', 'false']
    Description: Deploy the Cost and Usage Report stack?
  CURBucket:
    Description: The S3 bucket that has the source CUR files
    Type: String
  CURBucketRegion:
    Description: The S3 bucket region that has the source CUR files
    Type: String
    Default: us-east-1
  CURDataKeyPrefix:
    Description: The key prefix to use in the S3 path for CUR Data download
    Type: String
    Default: "cost_usage_report"
  CURDropColumns:
    Description: Comma separated list (string) of column names to drop from CUR file
    Type: String
    Default: ""

  GlueDataCatalogName:
    Description: The name of the datacatalog
    Type: String
    Default: "squeegee"
  GlueManagementRoleARN:
    Description: "The ARN of the role to use for managing Glue. (Default: new role created if 'create' supplied)"
    Type: String
    Default: "create"
  GlueCrawlerRoleARN:
    Description: "The ARN of the role to use for Glue Crawlers. (Default: new role created if 'create' supplied)"
    Type: String
    Default: "create"
  GlueRegion:
    Description: "The region to use for AWS Glue. (Default: 'same region' will use same region as cloudformation deployment)"
    Type: String
    Default: "same region"
  GlueCrawlerNamePrefix:
    Description: The prefix to use for Glue Crawler Names
    Type: String
    Default: "AWS-"
  GlueCrawlerTablePrefix:
    Description: The table prefix to use for datacatalog table names (Default none)
    Type: String
    Default: ""

  MutexTableName:
    Description: Name of the DynamoDB Table to use for Mutex Locks
    Type: String
    Default: "SqueegeeMutexTable"

  VPCId:
    Description: VPC Id of an existing VPC or set to "create" if you want Squegee to make one.
    Type: String
    AllowedPattern: "(^vpc-[0-9a-fA-F]{8}$|^create$)"
    Default: create
    ConstraintDescription: Must be the ID of a VPC or "create"
  Subnets:
    Description: Subnets where your batch nodes will run. MUST be within the selected VPC or "" if Squegee is creating the vpc for you. Subnets must be able to access the Internet (either directly or via NAT).
    Type: CommaDelimitedList
    Default: ""
    ConstraintDescription: Must be a list of two Subnet ID's within the selected VPC or empty if "create" VPC.

  DockerImageName:
    Description: The name of the docker image for use in ECS
    Type: String
    Default: "atlassiancloudteam/squeegee"

  SqueegeeS3BucketName:
    Type: String
    Default: "SQUEEGEE_DEPLOYMENT_BUCKET"
    Description: Location (bucket) of the templates
  SqueegeeS3Region:
    Type: String
    Default: us-east-1
    Description: Location (region) of the templates
  SqueegeeS3KeyPrefix:
    Type: String
    Default: "aws_billing_analytics_deployment/"
    Description: Location (prefix) of the templates

  CreateBatchLogGroup:
    Type: String
    Default: false
    AllowedValues: ['true', 'false']
    Description: Should Squeegee create the batch Log Group?
  BatchJSONLogging:
    Type: String
    Default: false
    AllowedValues: ['true', 'false']
    Description: Should Squeegee batch job log in JSON format?
  BatchMinCPU:
    Type: Number
    Default: 0
    Description: Minimum number of Batch vCPUs
  BatchMaxCPU:
    Type: Number
    Default: 64
    Description: Maximum number of Batch vCPUs
  BatchBidPercentage:
    Type: Number
    Default: 50
    Description: Bid percentage for Batch spot instances
  BatchJobvCPU:
    Type: Number
    Default: 1
    Description: Number of vCPU for the Batch Job
  BatchJobMemory:
    Type: Number
    Default: 256
    Description: Amount of Memory for the Batch Job


  DeployPricingDataStack:
    Type: String
    Default: false
    AllowedValues: ['true', 'false']
  PricingDataKeyPrefix:
    Type: String
    Default: "aws_pricing"
    Description: The key prefix to use in the S3 path for Pricing Data download

Mappings: {}

Conditions:
  DeployGlueManagementRole: !Equals [!Ref GlueManagementRoleARN, "create"]
  DeployGlueCrawlerRole: !Equals [!Ref GlueCrawlerRoleARN, "create"]
  SetGlueLocalRegion: !Equals [!Ref GlueRegion, "same region"]
  CreateVPC: !Equals [!Ref VPCId, "create"]
  DefaultSqueegeeBucket: !Equals [!Ref SqueegeeS3BucketName, "dbr-pp-deployments"]
  ShouldCreateBatchLogGroup: !Equals [!Ref CreateBatchLogGroup, "true"]
  ShouldDeployCURDataStack: !Equals [!Ref DeployCURDataStack, "true"]
  ShouldDeployPricingDataStack: !Equals [!Ref DeployPricingDataStack, "true"]
  GovCloudCondition: !Equals [!Ref "AWS::Region", "us-gov-west-1"]

Resources:
  ### Used by squeegee to send notifications to admins
  SqueegeeNotificationsSNS:
    Type: "AWS::SNS::Topic"
    Properties:
      DisplayName: SqueegeeNotifications
      TopicName: SqueegeeNotifications

  # VPC Stack created if VPC details not provided.
  VPCStack:
    Condition: CreateVPC
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Sub
          - https://${S3BucketName}.${S3Region}.amazonaws.com/${SqueegeeS3KeyPrefix}BITBUCKET_BUILD_NUMBER/templates/vpc.yaml
          -
            S3BucketName: !If [ DefaultSqueegeeBucket, !Sub "dbr-pp-deployments-${AWS::Region}", !Ref SqueegeeS3BucketName ]
            S3Region: !If [GovCloudCondition, "s3-us-gov-west-1", "s3" ]
      Parameters:
        AvailabilityZones: !Join [ ",", [ !Select [ 0, !GetAZs { "Ref" : "AWS::Region" } ], !Select [ 1, !GetAZs { "Ref" : "AWS::Region" } ]]]

  # Table used for Locking so Squeegee doesn't run the same processing concurrently
  MutexTable:
    Type: "AWS::DynamoDB::Table"
    Properties:
      AttributeDefinitions:
        - AttributeName: "lockname"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "lockname"
          KeyType: "HASH"
      ProvisionedThroughput:
        ReadCapacityUnits: "2"
        WriteCapacityUnits: "2"
      TableName: !Ref MutexTableName

  # Role used by Squeegee to crawl the analytics buckets for tables
  GlueCrawlerRole:
    Condition: DeployGlueCrawlerRole
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          Effect: "Allow"
          Principal:
            Service: "glue.amazonaws.com"
          Action: "sts:AssumeRole"
      Path: "/"
  GlueCrawlerRolePolicy:
    Condition: DeployGlueCrawlerRole
    Type: AWS::IAM::Policy
    DependsOn: GlueCrawlerRole
    Properties:
      Roles:
        - !Ref GlueCrawlerRole
      PolicyName: GluePolicy
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Action:
             - "logs:CreateLogGroup"
             - "logs:CreateLogStream"
             - "logs:PutLogEvents"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "glue:*"
              - "datacatalog:*"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "s3:ListAllMyBuckets"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "s3:GetBucketLocation"
              - "s3:GetObject"
              - "s3:PutObject"
              - "s3:ListBucket"
              - "s3:DeleteObject"
            Resource:
              - !Sub "arn:aws:s3:::${AnalyticsBucket}"
              - !Sub "arn:aws:s3:::${AnalyticsBucket}/*"

  # Role used by Squeegee to configure Glue Crawlers and trigger runs
  GlueManagementRole:
    Condition: DeployGlueManagementRole
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          Effect: "Allow"
          Principal:
            AWS:
              - !GetAtt LambdaExecutionRole.Arn
              - !GetAtt BatchJobRole.Arn
          Action: "sts:AssumeRole"
      Path: "/"
  GlueManagementRolePolicy:
    Condition: DeployGlueManagementRole
    Type: AWS::IAM::Policy
    DependsOn: GlueManagementRole
    Properties:
      Roles:
        - !Ref GlueManagementRole
      PolicyName: GluePolicy
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Action:
             - "logs:CreateLogGroup"
             - "logs:CreateLogStream"
             - "logs:PutLogEvents"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "glue:*"
              - "datacatalog:*"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "s3:ListAllMyBuckets"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "iam:PassRole"
            Resource: !If [DeployGlueCrawlerRole, !GetAtt GlueCrawlerRole.Arn, !Ref GlueCrawlerRoleARN]
            Condition:
              StringLike:
                "iam:PassedToService": "glue.amazonaws.com"

  # Base role used by All Squeegee Lambda Functions
  LambdaExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          Effect: "Allow"
          Principal:
            Service: "lambda.amazonaws.com"
          Action: "sts:AssumeRole"
      Path: "/"
  LambdaPolicy:
    Type: AWS::IAM::Policy
    DependsOn:
      - LambdaExecutionRole
      - BatchInstanceRole
    Properties:
      Roles:
        - !Ref LambdaExecutionRole
        - !Ref BatchInstanceRole
      PolicyName: LambdaPolicy
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action: "logs:CreateLogGroup"
            Effect: "Allow"
            Resource: "*"
          - Effect: "Allow"
            Action:
             - "logs:CreateLogStream"
             - "logs:PutLogEvents"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "xray:PutTraceSegments"
              - "xray:PutTelemetryRecords"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "dynamodb:PutItem"
              - "dynamodb:DescribeTable"
            Resource: !GetAtt MutexTable.Arn
          - Action:
             - "batch:DescribeJobs"
             - "batch:SubmitJob"
             - "batch:ListJobs"
            Effect: "Allow"
            Resource: "*"
          - Effect: Allow
            Action:
              - "sns:Publish"
            Resource: !Ref SqueegeeNotificationsSNS

  # Batch Resources

  ### Roles used by AWS Batch
  BatchServiceRole:
    Type: AWS::IAM::Role
    Properties:
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          Action: "sts:AssumeRole"
          Effect: "Allow"
          Principal:
            Service: "batch.amazonaws.com"
  BatchSpotFleetIamRole:
    Type: AWS::IAM::Role
    Properties:
        ManagedPolicyArns:
          - "arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
        AssumeRolePolicyDocument:
          Version: "2012-10-17"
          Statement:
              Action: "sts:AssumeRole"
              Effect: "Allow"
              Principal:
                Service: "spotfleet.amazonaws.com"
  BatchInstanceRole:
    Type: AWS::IAM::Role
    Properties:
        ManagedPolicyArns:
          - "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
        AssumeRolePolicyDocument:
          Version: "2008-10-17"
          Statement:
            Action: "sts:AssumeRole"
            Effect: "Allow"
            Principal:
              Service: "ec2.amazonaws.com"
  BatchInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
        - !Ref BatchInstanceRole
  BatchJobRole:
    Type: "AWS::IAM::Role"
    Properties:
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
            Effect: "Allow"
            Principal:
              Service:
                - "ecs-tasks.amazonaws.com"
                - "ecs.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      Path: "/"
  BatchJobRolePermissions:
    Type: AWS::IAM::Policy
    Properties:
      Roles:
        - !Ref BatchJobRole
      PolicyName: BasePolicy
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "s3:Get*"
              - "s3:List*"
            Effect: "Allow"
            Resource:
              - !Sub "arn:aws:s3:::${CURBucket}"
              - !Sub "arn:aws:s3:::${CURBucket}/*"
          - Action:
              - "s3:Get*"
              - "s3:List*"
              - "s3:PutObject"
              - "s3:DeleteObject"
            Effect: "Allow"
            Resource:
              - !Sub "arn:aws:s3:::${AnalyticsBucket}"
              - !Sub "arn:aws:s3:::${AnalyticsBucket}/*"
          - Effect: "Allow"
            Action:
              - "glue:*"
              - "datacatalog:*"
            Resource: "*"
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Resource: !If [ DeployGlueManagementRole, !GetAtt GlueManagementRole.Arn, !Ref GlueManagementRoleARN ]
          - Action:
              - "iam:PassRole"
            Effect: "Allow"
            Resource: !If [ DeployGlueCrawlerRole, !GetAtt GlueCrawlerRole.Arn, !Ref GlueCrawlerRoleARN ]
          - Action:
             - "batch:DescribeJobs"
             - "batch:SubmitJob"
             - "batch:ListJobs"
            Effect: "Allow"
            Resource: "*"
          - Effect: "Allow"
            Action:
              - "dynamodb:PutItem"
              - "dynamodb:DescribeTable"
            Resource: !GetAtt MutexTable.Arn
          - Effect: Allow
            Action:
              - "sns:Publish"
            Resource: !Ref SqueegeeNotificationsSNS

  ### Security Group for Batch Instances
  BatchSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Squeegee (AWS Batch SG)
      VpcId:
        !If
          - CreateVPC
          - !GetAtt VPCStack.Outputs.VPC
          - !Ref VPCId
  BatchSecurityGroupIngressFromSelf:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Ingress from other containers in the same security group
      GroupId: !Ref BatchSecurityGroup
      IpProtocol: -1
      SourceSecurityGroupId: !Ref BatchSecurityGroup

  ### CloudWatch Logs group
  BatchLogGroup:
    Condition: ShouldCreateBatchLogGroup
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: "/aws/batch/job"
      RetentionInDays: 7

  ### Batch Configuration
  SqueegeeBatchComputeEnvironment:
    Type: AWS::Batch::ComputeEnvironment
    Properties:
        Type: MANAGED
        ServiceRole: !Ref BatchServiceRole
        ComputeResources:
          Type: SPOT
          BidPercentage: !Ref BatchBidPercentage
          SpotIamFleetRole: !Ref BatchSpotFleetIamRole
          InstanceRole: !Ref BatchInstanceProfile
          MinvCpus: !Ref BatchMinCPU
          MaxvCpus: !Ref BatchMaxCPU
          DesiredvCpus: !Ref BatchMinCPU
          InstanceTypes:
            - optimal
          SecurityGroupIds:
            - !Ref BatchSecurityGroup
          Subnets:
            !If
              - CreateVPC
              - - !GetAtt VPCStack.Outputs.PublicSubnet1
                - !GetAtt VPCStack.Outputs.PublicSubnet2
              - - !Select [0, !Ref Subnets]
                - !Select [1, !Ref Subnets]
  SqueegeeBatchJobQueue:
    Type: AWS::Batch::JobQueue
    Properties:
        Priority: 1
        ComputeEnvironmentOrder:
          -
            ComputeEnvironment: !Ref SqueegeeBatchComputeEnvironment
            Order: 1
  BatchJobDefinition:
    Type: AWS::Batch::JobDefinition
    Properties:
      Type: container
      JobDefinitionName: SqueegeeRunner
      RetryStrategy:
        Attempts: 1
      ContainerProperties:
        JobRoleArn: !GetAtt BatchJobRole.Arn
        Image: !Sub "${DockerImageName}:BITBUCKET_BUILD_NUMBER"
        Memory: !Ref BatchJobMemory
        Vcpus: !Ref BatchJobvCPU
        Environment:
          - Name: CUR_BUCKET
            Value: !Ref CURBucket
          - Name: CUR_BUCKET_REGION
            Value: !Ref CURBucketRegion
          - Name: CUR_DATA_KEY_PREFIX
            Value: !Ref CURDataKeyPrefix
          - Name: CUR_DROP_COLUMNS
            Value: !Ref CURDropColumns
          - Name: ANALYTICS_BUCKET
            Value: !Ref AnalyticsBucket
          - Name: ANALYTICS_BUCKET_REGION
            Value: !Ref AnalyticsBucketRegion
          - Name: GLUE_MANAGEMENT_ROLE
            Value: !If
              - DeployGlueManagementRole
              - !GetAtt GlueManagementRole.Arn
              - !Ref GlueManagementRoleARN
          - Name: GLUE_CRAWLER_NAME_PREFIX
            Value: !Ref GlueCrawlerNamePrefix
          - Name: GLUE_CRAWLER_TABLE_PREFIX
            Value: !Ref GlueCrawlerTablePrefix
          - Name: GLUE_REGION
            Value: !If
              - SetGlueLocalRegion
              - !Ref "AWS::Region"
              - !Ref GlueRegion
          - Name: GLUE_CRAWLER_ROLE
            Value: !If
              - DeployGlueCrawlerRole
              - !GetAtt GlueCrawlerRole.Arn
              - !Ref GlueCrawlerRoleARN
          - Name: DATACATALOG_DB_NAME
            Value: !Ref GlueDataCatalogName
          - Name: DD_MUTEX_TABLE_NAME
            Value: !Ref MutexTableName
          - Name: SQUEEGEE_BUILD
            Value: "BITBUCKET_BUILD_NUMBER"
          - Name: BATCH_REGION
            Value: !Ref "AWS::Region"
          - Name: BATCH_JOB_QUEUE
            Value: !Ref SqueegeeBatchJobQueue
          - Name: BATCH_JSON_LOGGING
            Value: !Ref BatchJSONLogging
          - Name: MAPPINGS_PREFIX
            Value: "mappings"
          - Name: AWS_REGION
            Value: !Ref "AWS::Region"
          - Name: PRICING_DATA_KEY_PREFIX
            Value: !Ref PricingDataKeyPrefix
          - Name: SQUEEGEE_NOTIFICATIONS_SNS
            Value: !Ref SqueegeeNotificationsSNS

  # Squeegee Sub-Stacks

  ### Cost and Usage Report Stack
  CURStack:
    Condition: ShouldDeployCURDataStack
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Sub
          - https://${S3BucketName}.${S3Region}.amazonaws.com/${SqueegeeS3KeyPrefix}BITBUCKET_BUILD_NUMBER/templates/cur-data.yaml
          -
            S3BucketName: !If [ DefaultSqueegeeBucket, !Sub "dbr-pp-deployments-${AWS::Region}", !Ref SqueegeeS3BucketName ]
            S3Region: !If [GovCloudCondition, "s3-us-gov-west-1", "s3" ]
      Parameters:
        LambdaRoleARN: !GetAtt LambdaExecutionRole.Arn
        LambdaRoleID: !Ref LambdaExecutionRole
        CURTransformJobDefinition: !Ref BatchJobDefinition
        CURBucket: !Ref CURBucket
        BatchJobQueue: !Ref SqueegeeBatchJobQueue
        BatchJSONLogging: !Ref BatchJSONLogging

  ### Pricing Data Stack
  PricingStack:
    Condition: ShouldDeployPricingDataStack
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Sub
          - https://${S3BucketName}.${S3Region}.amazonaws.com/${SqueegeeS3KeyPrefix}BITBUCKET_BUILD_NUMBER/templates/pricing-data.yaml
          -
            S3BucketName: !If [ DefaultSqueegeeBucket, !Sub "dbr-pp-deployments-${AWS::Region}", !Ref SqueegeeS3BucketName ]
            S3Region: !If [GovCloudCondition, "s3-us-gov-west-1", "s3" ]
      Parameters:
        LambdaRoleARN: !GetAtt LambdaExecutionRole.Arn
        LambdaRoleID: !Ref LambdaExecutionRole
        CURTransformJobDefinition: !Ref BatchJobDefinition
        BatchJobQueue: !Ref SqueegeeBatchJobQueue
        BatchJSONLogging: !Ref BatchJSONLogging

Outputs: {}
