

---
AWSTemplateFormatVersion: "2010-09-09"

Description: Atlassian Squeegee VPC https://bitbucket.org/atlassian/squeegee/wiki/Home

Parameters:
  AvailabilityZones:
    Type: CommaDelimitedList
    Description: CommaDelimitedList of two Availability Zones (eg, "us-east-1a,us-east-1b")
  PrivateSubnet1CIDR:
    Type: String
    Default: "10.234.0.0/24"
    Description: The IP Pool for the first private subnet
  PrivateSubnet2CIDR:
    Type: String
    Default: "10.234.10.0/24"
    Description: The IP Pool for the second private subnet
  PublicSubnet1CIDR:
    Type: String
    Default: "10.234.20.0/24"
    Description: The IP Pool for the first public subnet
  PublicSubnet2CIDR:
    Type: String
    Default: "10.234.30.0/24"
    Description: The IP Pool for the second public subnet
  VPCCIDR:
    Type: String
    Default: "10.234.0.0/16"
    Description: The IP Pool for the VPC

Mappings: {}

Conditions: {}

Resources:
  # VPC Definition
  VPC:
    Type: "AWS::EC2::VPC"
    Properties:
      CidrBlock: !Ref VPCCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true

  # IGW Definition
  InternetGateway:
    Type: "AWS::EC2::InternetGateway"
  AttachVpnGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
        VpcId: !Ref VPC
        InternetGatewayId: !Ref InternetGateway

  # Route Table Definitions
  PrivateRouteTable:
    Type: "AWS::EC2::RouteTable"
    Properties:
        VpcId: !Ref VPC
  PublicRouteTable:
    Type: "AWS::EC2::RouteTable"
    Properties:
        VpcId: !Ref VPC

  # Route Definitions
  DefaultRoute:
    DependsOn: InternetGateway
    Type: AWS::EC2::Route
    Properties:
        RouteTableId: !Ref PublicRouteTable
        DestinationCidrBlock: 0.0.0.0/0
        GatewayId: !Ref InternetGateway

  # Subnet Definitions
  PrivateSubnet1:
    Type: "AWS::EC2::Subnet"
    Properties:
      CidrBlock: !Ref PrivateSubnet1CIDR
      AvailabilityZone: !Select [0, !Ref AvailabilityZones]
      MapPublicIpOnLaunch: false
      VpcId: !Ref VPC
  PrivateSubnet2:
    Type: "AWS::EC2::Subnet"
    Properties:
      CidrBlock: !Ref PrivateSubnet2CIDR
      AvailabilityZone: !Select [1, !Ref AvailabilityZones]
      MapPublicIpOnLaunch: false
      VpcId: !Ref VPC
  PublicSubnet1:
    Type: "AWS::EC2::Subnet"
    Properties:
      CidrBlock: !Ref PublicSubnet1CIDR
      AvailabilityZone: !Select [0, !Ref AvailabilityZones]
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
  PublicSubnet2:
    Type: "AWS::EC2::Subnet"
    Properties:
      CidrBlock: !Ref PublicSubnet2CIDR
      AvailabilityZone: !Select [1, !Ref AvailabilityZones]
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC

  # Subnet Attachments to Routetable Definitions
  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnet1
      RouteTableId: !Ref PublicRouteTable
  PublicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnet2
      RouteTableId: !Ref PublicRouteTable
  PrivateSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PrivateSubnet1
      RouteTableId: !Ref PrivateRouteTable
  PrivateSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PrivateSubnet2
      RouteTableId: !Ref PrivateRouteTable

Outputs:
  VPC:
    Description: VPC ID
    Value: !Ref VPC
    Export:
      Name: !Join [ ":", [ !Ref "AWS::StackName", "VPC" ] ]
  PrivateSubnet1:
    Description: Private subnet 1 ID
    Value: !Ref PrivateSubnet1
    Export:
      Name: !Join [ ":", [ !Ref "AWS::StackName", "PrivateSubnet1" ] ]
  PrivateSubnet2:
    Description: Private subnet 2 ID
    Value: !Ref PrivateSubnet2
    Export:
      Name: !Join [ ":", [ !Ref "AWS::StackName", "PrivateSubnet2" ] ]
  PublicSubnet1:
    Description: Public subnet 1 ID
    Value: !Ref PublicSubnet1
    Export:
      Name: !Join [ ":", [ !Ref "AWS::StackName", "PublicSubnet1" ] ]
  PublicSubnet2:
    Description: Public subnet 2 ID
    Value: !Ref PublicSubnet2
    Export:
      Name: !Join [ ":", [ !Ref "AWS::StackName", "PublicSubnet2" ] ]
